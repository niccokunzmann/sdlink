# sdlink

[![pipeline status](https://gitlab.com/niccokunzmann/sdlink/badges/main/pipeline.svg)](https://gitlab.com/niccokunzmann/sdlink/-/commits/main)

![](images/de-low-internal-storage.png)

Using UserLAnd and other Linux installations,
the Internal Memory is used up while the SD-card of the device is not used.
`sdlink` aims to solve this problem by transferring the content of files to the
SD-card while leaving the structure of the directories on the Internal Memory.

Even if you do not use an unrooted Android phone but a Linux
installation, sdlink may help you to transfer the weight of
your storage from one place to another.

Features:

- link directories recursively with one command
- executable files are not touched
- synchronization of
  - deleted and replaced files
  - new files and folders
  - moved files
Issues with using `sdlink`:
- `chmod` cannot make a symlink executable on the Android
  SD-card.
- If you move a link outside of the directory which sdlink
  manages, it will count as being deleted and synchronization
  will delete its content.

## When NOT to Use

If you can use `mount` to mount a ext2 formatted file,
that should be used as it preserves timestamps and
the executable bit.
You might be able to use mount when you have root
access or - in case of Android - when your phone is rooted.

If you cannot mount such a file, `sdlink` may be the
tool of your choice.
In case of Android, if your phone is not rooted.

## Installation for one User

This will do the following:
- download the scripts
- add them to "~/.local/bin"
- make them executable
- add `~/.local/bin` to your path in the `.bashrc file`.

```
mkdir -p "~/.local/bin"
cd "~/.local/bin"
wget https://niccokunzmann.gitlab.io/sdlink/sdlink
chmod +x sdlink
echo 'export PATH="$PATH:$HOME/.local/bin"' >> ~/.bashrc
source ~/.bashrc
```

## Installation for Everyone

You can put sdlink into your `$PATH` like this:

```
cd /usr/local/bin
sudo wget https://niccokunzmann.gitlab.io/sdlink/sdlink
sudo chmod +x sdlink
```

Now, it should work:
```
sdlink --help
```

## Usage

This section leads you through the different features which `sdlink` provides.

### Setting a Storage Directory

The storage is the directory to which sdlink will copy the
contents of the linked directories.
You can set a default storage by exporting the
`SDLINK_DEFAULT_STORAGE` environment variable:

For the [UserLAnd](https://f-droid.org/app/tech.ula) app, the SD-card can be found at
`/host-rootfs/sdcard/`.
This would be a configuration:
```
echo 'export SDLINK_DEFAULT_STORAGE=/host-rootfs/sdcard/sdlink' >> ~/.bashrc
```
Please note that it is advised to use a different storage
for each linux environment because `sdlink` does not
count who links to which file.

You can also define the storage each time you link a directory
using the `--storage` option.

### Linking a Directory

Link a directory and save some space:

```
sdlink --link ~/my-project
```

This copies the contents over to
`$SDLINK_DEFAULT_STORAGE/my-project` and
saves the configuration in `~/.config/sdlink`.

### Synchronizing After Changes

After you have changed files you can synchronize.

```
sdlink --sync-all
# or an absolute path
sdlink --sync "$HOME/my-project"
# or a relative path
sdlink --sync "./my-project"
```

Synchronization will
- link new files and directories
- delete content of files that were deleted
- continue if a specification was changed (`--link`, `--unlink`)
  and this synchronization was interrupted.

### Exclude Files and Directories from Linking

If you have some troubles with some files not being ok as links,
you can remove them:

```
sdlink --unlink ~/my-project/bin
```

Note that this will trigger a synchronization in the affected folders.

## More Options

You can pass more options to `sdlink` or set them as
environment variables.

- `--continue`
  Continue one interrupted copy process of a file.
- `--quiet` or `export SDLINK_QUIET=true`
  Run `sdlink` without any output.
- `--debug` or `export SDLINK_DEBUG=true`
  Run `sdlink` with much more output.
- `--help`
  Print a help message and exit.
- `--version`
  Print the version and exit.

## Restoring `sdlink` Directories And Links

In some cases, you might have manually destroyed the
directories which sdlink uses and you get error messages.
In any case, `sdlink` will not destroy content but rather
abort the synchronization.

You can run these commands:
```
sdlink --sync-all
sdlink --sync big-files
sdlink --sync "$SDLINK_DEFAULT_STORAGE/big-files"
```
These will try to restore all links that you may have
removed accidentially and proceed with linking and unlinking
files.
If you encounter a message that a copy process was interrupted,
use `--continue` additionally after you checked the files.

## Configuration

`sdlink` has different locations in which it saves its content.

- `SDLINK_DEFAULT_STORAGE` is the directory in which sdlink stores
  the directories that fill up space.
  This path should point to the SD storage of your phone, if you use a phone
  or any other location that has more space available.
  Recommendations:
  - UserLand: `/host-rootfs/storage/emulated/0/sdlink`
  - If you use another app, please create an Issue or Merge Request wih your
    recommended location.

  You can replace this storage for each directory you add with the `--storage`
  argument.
- `SDLINK_CONFIG` is the path to the directory in which `sdlink` stores
  all the different linked directories.
  When a directory is removed by the user, this allows `sdlink` to free up
  the rest of the storage.
  When `sdlink` updates all directories, the different storage locations
  can be considered.
  This path defaults to `$HOME/.config/sdlink`.

## How it Works

Suppose, you have the following directories:

- `SDLINK_DEFAULT_STORAGE` set to `/sdcard/sdlink` (e.g. `export SDLINK_DEFAULT_STORAGE="/sdcard/sdlink"` in `$HOME/.bashrc`)
- `~/big-files` is the directory which takes up a lot of space, with two files
  `podcast.ogg` and `download.zip`.

You run this command to free up space in your home directory:
```
sdlink --link ~/big-files
```

This has the following effect:
- `/sdcard/sdlink/big-files` is created.
- `/sdcard/sdlink/big-files/.source` links to `~/big-files`.
- `~/big-files/.sdlink` links to `/sdcard/sdlink/big-files`.
- `~/.config/sdlink` registers `/sdcard/sdlink/big-files`.
- `download.zip` and `podcast.ogg` are copied into `/sdcard/sdlink/big-files`.
- `~/big-files/download.zip` links to `/sdcard/sdlink/big-files/download.zip`.
- `~/big-files/podcast.ogg` links to `/sdcard/sdlink/big-files/podcast.ogg`.

If now, you add a file called `movie.mp4` to `~/big-files`, it is not
automatically linked. You can run this to save more space:

```
sdlink --sync-all
```

This has the following effect:
- `sdlink` uses `~/.config/sdlink` to look which directories to update.
- `sdlink` traverses `/sdcard/sdlink/big-files/.source` to find new files.
- `~/big-files/movie.mp4` is copied to `/sdcard/sdlink/big-files/movie.mp4`.
- `~/big-files/movie.mp4` links to `/sdcard/sdlink/big-files/movie.mp4`

If at some time, you remove the link `podcast.ogg`, `sdlink --sync-all` will
delete `/sdcard/sdlink/big-files/podcast.ogg`

If you decide to delete `~/big-files`, then `sdlink --sync-all`
deletes `/sdcard/sdlink/big-files`.

If you run `sdlink --unlink ~/big-files`, then all files will be copied
back and if that succeeds these will be removed:
- `~/big-files/.sdlink`
- `/sdcard/sdlink/big-files/.source`
- The reference in `~/.config/sdlink` to `/sdcard/sdlink/big-files`

You can decide to exempt directories like `~/big-files/keep`
from linking by running `sdlink --unlink ~/big-files/keep`.
This will create a file `~/big-files/keep/.sdlink` to mark the directory.

### Modification Time Stamps

If you use Android and the Android SD card, the modification times of the files
may get lost during the process.

## Development Setup

1. Clone the repository and install the scripts into the path:
   ```
   git clone https://gitlab.com/niccokunzmann/sdlink
   echo 'export PATH="$PATH:~/sdlink/bin/"' >> ~/.bashrc
   echo 'export SDLINK_DEFAULT_STORAGE="/host-rootfs/sdcard/sdlink"' >> ~/.bashrc
   source ~/.bashrc
   ```
   Now, you should be able to use `sdlink` on the command line.
2. Install [bashtest](https://gitlab.com/niccokunzmann/bashtest)
3. Run the tests
   ```
   bashtest
   ```

You can get additional output which is usually silenced:
```
bashtest --loud
```

## List of Used Software

- `realpath`
- `readlink`
- `bash` - support it!
- `basename`
- `dirname`
- `find`
- `cat`

## Development

`sdlink` is written in bash.
We use test-driven development with `bashtest`.
This has several advantages, one of them is that we only ship
releases when they work and we know that issues are closed.
If you find an issue in the software, please report it.
It helps even more, if you describe the setup and
the commands that can be used to re-create the
problematic state.
This way we can write a test and fix it for future releases.

## References and Related Work

See also
- https://github.com/EXALAB/AnLinux-App/issues/85#issuecomment-594995824
- https://github.com/CypherpunkArmory/UserLAnd/issues/160

